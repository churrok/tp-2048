package test;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import Juego.Juego;

class JuegoTest {

	@Test
	public void moverIzq(){
		
	Juego juego= new Juego();
	juego.tablero[2][0]=2;
	juego.tablero[2][1]=2;
	juego.tablero[2][2]=2;
	juego.tablero[2][3]=2;
	
	juego.moverIzq();
	
	
	assertTrue( juego.tablero[2][1]==4);
		
	}

	@Test
	public void moverDer() {
		
		Juego juego= new Juego();
		juego.tablero[2][0]=2;
		juego.tablero[2][1]=2;
		juego.tablero[2][2]=2;
		juego.tablero[2][3]=2;
		
		juego.moverDer();
		
		assertTrue( juego.tablero[2][3]==4);
		
		
	}

	@Test
	public void perdido()
	{
		
		Juego juego= new Juego();
		juego.tablero[0][0] = 2;
        juego.tablero[1][0] = 8;
        juego.tablero[2][0] = 4;
        juego.tablero[3][0] = 8;

        juego.tablero[0][1] = 2;
        juego.tablero[0][2] = 2;
        juego.tablero[0][3] = 2;

        juego.tablero[2][1] = 4;
        juego.tablero[2][2] = 4;
        juego.tablero[2][3] = 4;
        
        assertTrue(!juego.perdido());
	}

	@Test
	public void perdido2()
	{
		Juego juego= new Juego();
		int x=1;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				
				juego.tablero[i][j]=x;
				x++;
				
			}
		}
		
		assertTrue(juego.perdido());
	}
}
