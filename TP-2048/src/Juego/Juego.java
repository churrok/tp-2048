package Juego;

import java.awt.List;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.Random;

public class Juego 
{
	
	public int[][] tablero;
	public int colum = 4;
	public int filas = 4;
	
	public Juego(){
		
		tablero=new int[colum][filas];
		agregarNum();
		agregarNum();
		
	}
	public void printTablero() {
        String output = "";
        for (int i = 0; i < 4; i++) {
            String row = "";
            for (int j = 0; j < 4; j++) {
                row += tablero[i][j] + ",";
            }
            row += "\n";
            output += row;
        }
        System.out.println(output);
    }	
	public void moverIzq() {
		int indice;
		int numero=-1;
		for (int i = 0; i < filas; i++) {
			indice=-1;
			numero=-1;
			for (int j = 0; j < colum; j++) {
				if(tablero[i][j]!=0) {
					if(numero==tablero[i][j]) {
						tablero[i][indice]=numero*2;
						tablero[i][j]=0;
		                numero=-1;
					}
					else {
						numero=tablero[i][j];
						indice++;
						
						tablero[i][j]=0;
						tablero[i][indice]=numero;
					}
				}
				
			}
		}
	}
	public void moverDer() 
	{
		int indice;
		int numero=-1;
		for (int i = 0; i < filas; i++) {
			indice=colum;
			numero=-1;
			for (int j = colum-1; j >= 0; j--) {
				if(tablero[i][j]!=0) {
					if(numero==tablero[i][j]) {
						tablero[i][indice]=numero*2;
						tablero[i][j]=0;
		                numero=-1;
					}
					else {
						numero=tablero[i][j];
						indice--;
						
						tablero[i][j]=0;
						tablero[i][indice]=numero;
					}
				}
				
			}
		}	
	}
	public void moverArriba() {
		int indice;
		int numero=-1;
		for (int j = 0; j < colum; j++) {
			indice=-1;
			numero=-1;
			for (int i = 0; i < filas; i++) {
				if(tablero[i][j]!=0) {
					if(numero==tablero[i][j]) {
						tablero[indice][j]=numero*2;
						tablero[i][j]=0;
		                numero=-1;
					}
					else {
						numero=tablero[i][j];
						indice++;
						
						tablero[i][j]=0;
						tablero[indice][j]=numero;
					}
				}
				
			}
		}
	}
	public void moverAbj()
	{
		int indice;
		int numero=-1;
		for (int j = 0; j < colum; j++) {
			indice=colum;
			numero=-1;
			for (int i = filas-1; i >= 0; i--) {
				if(tablero[i][j]!=0) {
					if(numero==tablero[i][j]) {
						tablero[indice][j]=numero*2;
						tablero[i][j]=0;
		                numero=-1;
					}
					else {
						numero=tablero[i][j];
						indice--;
						
						tablero[i][j]=0;
						tablero[indice][j]=numero;
					}
				}
				
			}
		}
		
	}
	public boolean perdido()
	{
		int numero=-1;
		boolean iguales=false;
	for (int i = 0; i < colum; i++) {
		for (int j = 0; j < filas; j++) {
			if(tablero[i][j]==0)
				return false;
			else {
				iguales= iguales||numero==tablero[i][j];
				numero=tablero[i][j];
			}
			
			
		}
		numero=-1;
		for (int j = 0; j < tablero.length; j++) {
			for (int j2 = 0; j2 < tablero.length; j2++) {
				iguales= iguales||numero==tablero[i][j];
				numero=tablero[i][j];
			}
		}
	}
	return !iguales;
	}
    public void agregarNum() 
    {
    	Point2D pocision;
    	Random rand = new Random();
    	int n;
    	boolean ceros=false;
    	ArrayList<Point2D> pocisiones = new ArrayList<Point2D>();
    	for (int i = 0; i < tablero.length; i++) {
    		for (int j = 0; j < tablero.length; j++) {
				if(tablero[i][j]==0) {
					ceros=ceros || tablero[i][j]==0;
					pocision= new Point2D.Double((double)i,(double)j);
					pocisiones.add(pocision);
				}
			}
			
		}
    	n= rand.nextInt(pocisiones.size());
    	pocision=pocisiones.get(n);
    	if(ceros) {
    	if(n%2==0)
    		tablero[(int) pocision.getX()][(int) pocision.getY()]=2;
    	else
    		tablero[(int) pocision.getX()][(int) pocision.getY()]=4;
    	}
    }
    public boolean ganado() 
    {
    	for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				if(tablero[i][j]==2048)return true;
			}
		}
    	return false;
    }
    public int[][] tablero()
    {
    	return this.tablero;
    }
    public void limpiarTablero() 
    {
    	for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				tablero[i][j]=0;
			}
		}
    }
    public boolean puedeDer()
    {
    	int numero = -1;
    	boolean iguales=false;
    	for (int i = 0; i < tablero.length; i++) {
			for (int j = tablero.length - 1; j >= 0; j--) {
				if(tablero[i][j]>0 && numero==0) {
					return true;
				}
				iguales = iguales || tablero[i][j]==numero;
				numero = tablero[i][j];
			}
		}
    	return iguales;
    }
}