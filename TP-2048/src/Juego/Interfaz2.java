package Juego;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Interfaz2 {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz2 window = new Interfaz2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Juego juego= new Juego();
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		JButton[][] cuadros= new JButton[4][4];
		int x;
		int y = 0;
		for (int i = 0; i < cuadros.length; i++) {
			
			x=53;
			for (int j = 0; j < cuadros.length; j++) {
				cuadros[i][j]=new JButton("");
				cuadros[i][j].setFont(new Font("Calibri", Font.PLAIN, 20));
				cuadros[i][j].setBounds(x, y, 70, 70);
				frame.getContentPane().add(cuadros[i][j]);
				x = x + 70;
			}
			y= y + 70;
		}
		cargarJuego(juego.tablero(),cuadros);
		
		JButton btnNewButton = new JButton("mover derecha");
		btnNewButton.setBackground(Color.DARK_GRAY);
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(juego.puedeDer()) {
				
					juego.moverDer();
					juego.agregarNum();
					cargarJuego(juego.tablero(),cuadros);
				}
			}
		});
		btnNewButton.setBounds(213, 316, 120, 23);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("mover izquierda");
		btnNewButton_1.setBackground(Color.DARK_GRAY);
		btnNewButton_1.setForeground(Color.WHITE);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					juego.moverIzq();
					juego.agregarNum();
					cargarJuego(juego.tablero(),cuadros);
				
			}
		});
		btnNewButton_1.setBounds(53, 316, 120, 23);
		frame.getContentPane().add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("mover arriba");
		btnNewButton_2.setBackground(Color.DARK_GRAY);
		btnNewButton_2.setForeground(Color.WHITE);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				juego.moverArriba();
				
				juego.agregarNum();
				cargarJuego(juego.tablero(),cuadros);
			}
			
		});
		btnNewButton_2.setBounds(53, 282, 280, 23);
		frame.getContentPane().add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("mover abajo");
		btnNewButton_3.setBackground(Color.DARK_GRAY);
		btnNewButton_3.setForeground(Color.WHITE);
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				juego.moverAbj();
				
				juego.agregarNum();
				cargarJuego(juego.tablero(),cuadros);
				
			}
		});
		btnNewButton_3.setBounds(53, 350, 280, 23);
		frame.getContentPane().add(btnNewButton_3);
		
		JButton btnNewButton_4 = new JButton("reset");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				juego.limpiarTablero();
				juego.agregarNum();
				juego.agregarNum();
				cargarJuego(juego.tablero(),cuadros);
				
			}
		});
		btnNewButton_4.setBounds(53, 384, 120, 23);
		frame.getContentPane().add(btnNewButton_4);
		
		
	}
	private void cargarJuego(int[][] tablero,JButton[][] cuadros )
	{
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero.length; j++) {
				cuadros[i][j].setText(Integer.toString(tablero[i][j]));
			}
		}
	}
}
