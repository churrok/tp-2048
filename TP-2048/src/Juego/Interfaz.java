package Juego;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;

public class Interfaz {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel Label00 = new JLabel("New label");
		Label00.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label00.setBounds(85, 55, 105, 53);
		frame.getContentPane().add(Label00);
		
		JLabel Label01 = new JLabel("New label");
		Label01.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label01.setBounds(279, 55, 105, 53);
		frame.getContentPane().add(Label01);
		
		JLabel Label02 = new JLabel("New label");
		Label02.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label02.setBounds(463, 55, 105, 53);
		frame.getContentPane().add(Label02);
		
		JLabel Label03 = new JLabel("New label");
		Label03.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label03.setBounds(653, 55, 105, 53);
		frame.getContentPane().add(Label03);
		
		JLabel Label10 = new JLabel("New label");
		Label10.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label10.setBounds(85, 219, 105, 53);
		frame.getContentPane().add(Label10);
		
		JLabel Label11 = new JLabel("New label");
		Label11.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label11.setBounds(279, 219, 105, 53);
		frame.getContentPane().add(Label11);
		
		JLabel Label12 = new JLabel("New label");
		Label12.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label12.setBounds(463, 219, 105, 53);
		frame.getContentPane().add(Label12);
		
		JLabel Label13 = new JLabel("New label");
		Label13.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label13.setBounds(653, 219, 105, 53);
		frame.getContentPane().add(Label13);
		
		JLabel Label20 = new JLabel("New label");
		Label20.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label20.setBounds(85, 383, 105, 53);
		frame.getContentPane().add(Label20);
		
		JLabel Label21 = new JLabel("New label");
		Label21.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label21.setBounds(279, 383, 105, 53);
		frame.getContentPane().add(Label21);
		
		JLabel Label22 = new JLabel("New label");
		Label22.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label22.setBounds(463, 383, 105, 53);
		frame.getContentPane().add(Label22);
		
		JLabel Label23 = new JLabel("New label");
		Label23.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label23.setBounds(653, 383, 105, 53);
		frame.getContentPane().add(Label23);
		
		JLabel Label30 = new JLabel("New label");
		Label30.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label30.setBounds(85, 492, 105, 53);
		frame.getContentPane().add(Label30);
		
		JLabel Label31 = new JLabel("New label");
		Label31.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label31.setBounds(279, 492, 105, 53);
		frame.getContentPane().add(Label31);
		
		JLabel Label32 = new JLabel("New label");
		Label32.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label32.setBounds(463, 492, 105, 53);
		frame.getContentPane().add(Label32);
		
		JLabel Label33 = new JLabel("New label");
		Label33.setFont(new Font("Calibri", Font.PLAIN, 20));
		Label33.setBounds(653, 492, 105, 53);
		frame.getContentPane().add(Label33);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(85, 306, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		
	}
}
